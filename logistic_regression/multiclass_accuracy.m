% Returns the accuracy of multiclass classifier W
% W: (d+1) x c
% X: (d+1) x n
% y: 1 x n
function acc = multiclass_accuracy(W, X, y)

temp = sigmoid(W' * X);
pred = zeros(size(y));

n = length(y);
for i = 1:n
    [max_prob,index_y] = max(temp(:,i));
    pred(i) = index_y;
end

nacc = sum(y == pred);
acc = nacc / length(y);
