addpath helper

images = load_mnist_images('data/train-images-idx3-ubyte');
labels = load_mnist_labels('data/train-labels-idx1-ubyte');
display_network(images(:,1:100)); % print first 100 images

% 1. size of each image: 784
img_size = length(images(:,1:1));

% 2. range of labels: 0-9
label_range = [min(labels) max(labels)];

% 3. range of pixel values: 0-1
pixel_range = [min(images(:)) max(images(:))];

% 4. maximum and minimum l2-norm of the images: 3.5698-17.1790
l2_norms = sqrt(sum(images.^2,1));
l2_norm_range = [min(l2_norms) max(l2_norms)];

% 5. whether the data is sparse or dense: the data is dense, since 80.88%
% of all the pixels are zeros
row = length(images(:,1));
col = length(images(1,:));
a = sum(images(:)==0) / (row * col);

% 6. whether the label distribution is skewed or uniform: almost uniform
hist(labels);
