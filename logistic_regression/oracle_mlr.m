% Oracle mlr returns the function and gradient evaluated at w. 
% W: (d+1) x c
% X: (d+1) x n
% y: 1 x n
function [f, g] = oracle_mlr(W, X, y)

g = zeros(size(W));
Lambda = ones(size(W)) * 10;
Lambda(1,:) = 0; % do not penalize the bias term

% Y(i,j) = 1 if y(j) == i, otherwise 0
Y = full(sparse(y, 1:length(y), 1)); % c x n

temp_exp = exp(W' * X);
temp_denominator = sum(temp_exp, 1);
temp_numerator = sum(Y .* temp_exp, 1);
temp_regularization = Lambda .* (W.^2);
f = sum(log(temp_numerator ./ temp_denominator)) - 0.5 * sum(temp_regularization(:));


temp = zeros(size(temp_exp));
for i = 1:length(temp_denominator)
    temp(:,i) = temp_exp(:,i) ./ temp_denominator(i);
end
g = X * (Y - temp)' - (Lambda .* W);
