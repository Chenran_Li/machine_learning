% Returns the function and gradient evaluated at w. 
% w: (d+1) x 1
% X: (d+1) x n
% y: 1 x n
function [f, g] = oracle_lr(w, X, y)
f = 0;
g = zeros(size(w));
lambda = ones(size(w)) * 10; % used in Q2.2.7
lambda(1) = 0; % do not penalize the bias term

temp = sigmoid(w' * X);
%f = sum(y .* log(temp) + (1-y) .* log(1-temp) ) - 0.5 * sum(lambda .* (w.^2));
f = sum(y .* log(temp) + (1-y) .* log(1-temp) );

%g = ((y-temp) * X')' - (lambda .* w);
g = ((y-temp) * X')';



