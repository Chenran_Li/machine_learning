function err = grad_check(oracle, t, varargin)

h = 1e-6;
d = size(t,1);
c = size(t,2);

[f,g] = oracle(t, varargin{:});
%g_hat = zeros(d,1);
g_hat = zeros(size(t));

for i = 1:d
    for j = 1:c
        h_e = zeros(size(t));
        h_e(i,j) = h;
        [f1,g1] = oracle(t+h_e, varargin{:});
        h_e(i,j) = -h;
        [f2,g2] = oracle(t+h_e, varargin{:});
        g_hat(i,j) = (f1-f2) / (2*h);
    end
end

err = sum(abs(g_hat(:)-g(:))) / d;
