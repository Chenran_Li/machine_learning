function err = grad_check(oracle, t, varargin)

h = 1e-6;
d = length(t);

[f,g] = oracle(t, varargin{:});
g_hat = zeros(d,1);

for i = 1:d
    h_e = zeros(d,1);
    h_e(i) = h;
    [f1,g1] = oracle(t+h_e, varargin{:});
    h_e(i) = -h;
    [f2,g2] = oracle(t+h_e, varargin{:});
    g_hat(i) = (f1-f2) / (2*h);
end

err = sum(abs(g_hat-g)) / d;
