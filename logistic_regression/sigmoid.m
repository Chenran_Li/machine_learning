function s = sigmoid(a)
    s = ones(size(a)) ./ (1 + exp(-a));

