# -*- coding: utf-8 -*-
import os
import math

MODEL = 'normal'
#MODEL = 'negation'
#MODEL = 'bi-gram'
PREFIXES = set(['extremely', 'quite', 'just', 'almost', 'very', 'too', 'enough'])
WORD_FILE = os.path.join('data', 'words.txt')
LABELS = ['neg', 'pos']
DIR = {}
for label in LABELS:
    DIR[label] = os.path.join('data', label)

def load_vocab():
    if MODEL == 'normal':
        vocab = {}
        with open(WORD_FILE) as f:
            for line in f.readlines():
                line = line.strip('\n').strip(' ')
                if len(line) > 0:
                    dic = {}
                    for label in LABELS:
                        dic[label] = 0.1
                    vocab[line] = dic
        return vocab
    elif MODEL == 'negation':
        vocab = {}
        with open(WORD_FILE) as f:
            for line in f.readlines():
                line = line.strip('\n').strip(' ')
                if len(line) > 0:
                    dic = {}
                    for label in LABELS:
                        dic[label] = 0.1
                    vocab[line] = dic
        for label in LABELS:
            cur_label_file_list = os.listdir(DIR[label])
            for cur_file in cur_label_file_list:
                file_name = os.path.join(DIR[label], cur_file)
                if int(cur_file[2:5]) < 800:
                    with open(file_name) as f:
                        for line in f.readlines():
                            line = line.strip('\n').strip(' ').split(' ')
                            if len(line) <= 1:
                                continue
                            for i in range(0, len(line)-1):
                                if line[i] == 'not' or line[i] == 'n\'t':
                                    if ('not_'+line[i+1]) in vocab:
                                        continue
                                    dic = {}
                                    dic['neg'] = 0.1
                                    dic['pos'] = 0.1
                                    vocab['not_'+line[i+1]] = dic
        return vocab
    else:
        vocab = {}
        with open(WORD_FILE) as f:
            for line in f.readlines():
                line = line.strip('\n').strip(' ')
                if len(line) > 0:
                    dic = {}
                    for label in LABELS:
                        dic[label] = 0.1
                    vocab[line] = dic
                    for prefix in PREFIXES:
                        dic = {}
                        for label in LABELS:
                            dic[label] = 0.1
                        vocab[prefix + '_' + line] = dic
        return vocab

def get_text_tokens(file_name, vocab):
    if MODEL == 'normal':
        tokens = []
        with open(file_name) as f:
            for line in f.readlines():
                line = line.strip('\n').strip(' ').split(' ')
                tokens += line
        token_set = set()
        for token in tokens:
            if token in vocab:
                token_set.add(token)
        return token_set
    elif MODEL == 'negation':
        tokens = []
        token_set = set()
        with open(file_name) as f:
            for line in f.readlines():
                line = line.strip('\n').strip(' ').split(' ')
                if len(line) > 1:
                    for i in range(0, len(line)-1):
                        if line[i] == 'not' or line[i] == 'n\'t':
                            token_set.add('not_'+line[i+1])
                tokens += line
        for token in tokens:
            if token in vocab:
                token_set.add(token)
        return token_set
    else:
        tokens = []
        token_set = set()
        with open(file_name) as f:
            for line in f.readlines():
                line = line.strip('\n').strip(' ').split(' ')
                if len(line) > 1:
                    for i in range(0, len(line)-1):
                        if line[i] in PREFIXES:
                            cur_token = line[i] + '_' + line[i+1]
                            if cur_token in vocab:
                                token_set.add(cur_token)
                tokens += line
        for token in tokens:
            if token in vocab:
                token_set.add(token)
        return token_set

def get_train_set(vocab):
    train_set = {}
    for label in LABELS:
        train_set[label] = []
        cur_label_file_list = os.listdir(DIR[label])
        for cur_file in cur_label_file_list:
            file_name = os.path.join(DIR[label], cur_file)
            if int(cur_file[2:5]) < 800:
                tokens = get_text_tokens(file_name, vocab)
                train_set[label].append(tokens)
    return train_set

def get_test_set(vocab):
    test_set = {}
    for label in LABELS:
        test_set[label] = []
        cur_label_file_list = os.listdir(DIR[label])
        for cur_file in cur_label_file_list:
            file_name = os.path.join(DIR[label], cur_file)
            if int(cur_file[2:5]) >= 800:
                tokens = get_text_tokens(file_name, vocab)
                test_set[label].append(tokens)
    return test_set

def train_model(vocab, train_set):
    model = {}
    total = 0
    counter = {}
    for label in LABELS:
        model[label] = len(train_set[label])
        counter[label] = 0
        total += len(train_set[label])
        for tokens in train_set[label]:
            for token in tokens:
                vocab[token][label] += 1
                counter[label] += 1
    for label in LABELS:
        for word in vocab:
            vocab[word][label] = float(vocab[word][label]) / ( counter[label] + 0.1 * len(vocab) )
        model[label] = float(model[label]) / total
    model['vocab'] = vocab

    counter = 0
    for label in LABELS:
        for word in vocab:
            counter += vocab[word][label]
        print counter
    return model

def predict(model, doc):
    prob_dict = {}
    vocab = model['vocab']
    for label in LABELS:
        prob = math.log(model[label])
        for token in doc:
            if token in vocab:
                prob += math.log(vocab[token][label])
        prob_dict[label] = prob

    if prob_dict['neg'] > prob_dict['pos']:
        return 'neg'
    return 'pos'

def test_model(model, test_set):
    total = 0
    hit = 0
    for label in LABELS:
        total += len(test_set[label])
        for cur_doc in test_set[label]:
            res = predict(model, cur_doc)
            if res == label:
                hit += 1
    accuracy = float(hit) / total
    return total, hit, accuracy

def get_top_words(model):
    vocab = model['vocab']
    top_words = {}
    for label in LABELS:
        words = vocab.keys()
        words.sort( key = lambda x: -vocab[x][label] )
        top_words[label] = words
    return top_words

if __name__ == "__main__":
    vocab = load_vocab()
    train_set = get_train_set(vocab)
    test_set = get_test_set(vocab)
    model = train_model(vocab, train_set)
    test_res_train = test_model(model, train_set)
    test_res_test = test_model(model, test_set)
    top_words = get_top_words(model)