# -*- coding: utf-8 -*-

import math

def log2(a):
    return math.log(a) / math.log(2)

with open( 'c45data.csv' ) as f:
    lines = f.readlines()
    labels = lines[-1].strip(' ').lower().split(' ')
    output = []
    output.append( lines[0].strip('\n').strip(' ') )
    for i in range(1, len(lines)-1):
        output.append(lines[i].strip('\n').strip(' ').lower().replace(' ',',') + ',' +labels[i-1])

def compute( instance_id ):
    zero = 0
    one = 0
    for i in instance_id:
        label = points[i]['play']
        if label == 'yes':
            one += 1
        else:
            zero += 1
    if one == 0 or zero == 0:
        return 0
    total = len(instance_id)
    one = float(one) / total
    zero = float(zero) / total
    HX = -one * log2(one) - zero * log2(zero)
    return HX

features = output[0].split(',')[:-1]
points = [[]]
for point in output[1:]:
    point = point.split(',')
    point_dict = {}
    for i in range(4):
        point_dict[features[i]] = point[i]
    point_dict['play'] = point[4]
    points.append(point_dict)


instance_id = [1,2,3,4,5,6,7,8,9,10,11,12,13,14]

def split_point(instance_id, feature):
    dic = {}
    for ins in instance_id:
        if points[ins][feature] not in dic:
            dic[points[ins][feature]] = []
        dic[points[ins][feature]].append(ins)
    return dic.values(), dic

def generate_features(fea, features):
    res = []
    for f in features:
        if not f == fea:
            res.append(f)
    return res

def comp_split_info(num_samples, groups):
    res = [len(a)/num_samples for a in groups]
    res = map( lambda x: -x * log2(x), res )
    return reduce(lambda x, y: x + y, res)

def same(ids):
    label = points[ids[0]]['play']
    for cur_id in ids:
        if points[cur_id]['play'] != label:
            return False
    return True

def build_tree(instance_id, features):
    if same(instance_id):
        return []
    if len(features) == 0:
        return []
    min_feature = ''
    min_HX = -1
    best_split = []
    cur_HY = compute(instance_id)
    for fea in features:
        groups, cur_split = split_point(instance_id, fea)
        if len(groups) == 1:
            continue
        cur_HX = 0
        total_ins_num = float(len(instance_id))
        for group in groups:
            this_ins_num = len(group)
            cur_HX += this_ins_num / total_ins_num * compute(group)
        IG_X = cur_HY - cur_HX
        IG_X /= comp_split_info(total_ins_num, groups)
        if IG_X >= min_HX:
            min_feature = fea
            min_HX = IG_X
            best_split = cur_split
    if min_HX == -1:
        return []
    node = instance_id, min_feature, best_split
    nodes = []
    nodes.append(node)
    if len(best_split) == 1:
        return node
    new_features = generate_features(min_feature, features)
    for group in best_split.values():
        nodes.append(build_tree(group, new_features))
    return nodes

anodes = build_tree(instance_id, features)